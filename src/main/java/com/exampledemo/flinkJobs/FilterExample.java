package com.exampledemo.flinkJobs;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;

public class FilterExample {

  public static void apply(StreamExecutionEnvironment env) {
    final DataStream<Integer> input = env.fromElements(1, 2, 2, 3, 4, 5);

    final DataStream<Integer> filtered = input.filter(new GreaterThan2());

    filtered.print();
  }

  public static class GreaterThan2 implements FilterFunction<Integer> {
    @Override
    public boolean filter(Integer value) {
        return value > 2;
    }
  }
}
