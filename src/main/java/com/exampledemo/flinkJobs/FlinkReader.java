package com.exampledemo.flinkJobs;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;


public class FlinkReader {
  public static void apply(StreamExecutionEnvironment env) {
    final String dirPath = "/tmp/streaming_source/files/";
    /*1. Lectura */
    final DataStream<String> text = env.readTextFile(dirPath);

    /*2. Escritura en consola */
    text.print();
  }
}
