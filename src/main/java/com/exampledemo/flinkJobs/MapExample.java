package com.exampledemo.flinkJobs;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;

import java.util.logging.Logger;

public class MapExample {

  public void apply(StreamExecutionEnvironment env) {

    final DataStream<String> input = env.fromElements(
        "Uno",
        "Dos",
        "Tres",
        "Cuatro"
        );

    final DataStream<String> numbered = input.map(new IntAdder());

    final StreamingFileSink<String> sink = StreamingFileSink
        .forRowFormat(
            new Path("src/resources/sink"),
            new SimpleStringEncoder<String>("UTF-8"))
        .build();

    numbered.addSink(sink);
  }

  public static class IntAdder implements MapFunction<String, String> {
    public String map(String value) {
      return String.format("%s: %s", value, 1);
    }
  }
}
