package com.exampledemo;

import com.exampledemo.flinkJobs.FilterExample;
import com.exampledemo.flinkJobs.FlinkReader;
import com.exampledemo.flinkJobs.FlinkWordCounter;
import com.exampledemo.flinkJobs.MapExample;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.logging.Logger;

public class DemoJob {
  static final StreamExecutionEnvironment streamEnv = StreamExecutionEnvironment.getExecutionEnvironment();
  static final ExecutionEnvironment execEnv = ExecutionEnvironment.getExecutionEnvironment();

  public static void main(String[] args) throws Exception {
    final Logger logger = Logger.getLogger(DemoJob.class.getName());

    streamEnv.setParallelism(1);
    execEnv.setParallelism(1);

    /*Simple lectura en consola*/
    logger.info("=======> Lectura en consola");
    FlinkReader.apply(streamEnv);

    /*WordCount de documentación*/
    logger.info("=======> WordCount");
    FlinkWordCounter.apply(execEnv);

    /*Map example*/
    logger.info("=======> Ejemplo de Map");
    MapExample map = new MapExample();
    map.apply(streamEnv);

    /*Filter Example*/
    logger.info("=======> Ejemplo de Filtetr");
    FilterExample.apply(streamEnv);

    streamEnv.execute();
  }
}
